/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController} from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Credit } from '../../models/Credit';

/********************* Service ************************/
import { CreditService } from '../../services/CreditService';



@Component({
  selector: 'page-credit-form',
  templateUrl: 'credit-form.html'
})
export class CreditFormPage {

  //libels and holdplaces
  page_title : string;
  title_placeholder : string;
  sum_placeholder : string;
  description_placeholder : string;
  endMonth_placeholder : string;
  endYear_placeholder : string;

  //Info
  title : string ;
  sum : number ;
  description : string;
  endMonth : number;
  endYear : number;

  // Credit to update
  credit : Credit = new Credit();

  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public creditService: CreditService, 
              public translateService: TranslateService,
              public toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  _render(){
  	this.translateService.get('NEW_CREDIT_TITLE').subscribe( value => { this.page_title = value; });
  	this._renderForm();
  }

  _renderForm(){
    this.translateService.get('T_PLACEHOLDER').subscribe( value => { this.title_placeholder = value; });
	this.translateService.get('SUM_PLACEHOLDER').subscribe( value => { this.sum_placeholder = value; });
    this.translateService.get('DESCRIPTION_PLACEHOLDER').subscribe( value => { this.description_placeholder = value; });
    this.translateService.get('END_MONTH_PLACEHOLDER').subscribe( value => { this.endMonth_placeholder = value; });
    this.translateService.get('END_YEAR_PLACEHOLDER').subscribe( value => { this.endYear_placeholder = value; });
    this._renderValues();
  }

  _renderValues(){
    if(this.navParams.get('credit') !== null){
      this.credit = this.navParams.get('credit');
      this.title = this.credit.getTitle();
      this.sum = this.credit.getSum();
      this.description = this.credit.getDescription();
      this.endMonth = this.credit.getEndMonth();
      this.endYear = this.credit.getEndYear();
    } 
  }

  _loadModel(){
    this.credit.setTitle(this.title);
    this.credit.setSum(this.sum);
    this.credit.setDescription(this.description);
    this.credit.setEndMonth(this.endMonth);
    this.credit.setEndYear(this.endYear);
  }

  save(){
    this._loadModel();
    this.creditService.save(this.credit)
      .then( () => { 
        this.translateService.get('CREDIT_CREATED').subscribe( value => { this._lunchToast(value) });
      })
      .catch( (err) => { console.error('Errooor', err); });
    this.navCtrl.pop(this);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
