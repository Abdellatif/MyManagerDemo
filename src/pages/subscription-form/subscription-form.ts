/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController} from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Subscription } from '../../models/Subscription';

/********************* Service ************************/
import { SubscriptionService } from '../../services/SubscriptionService';



@Component({
  selector: 'page-subscription-form',
  templateUrl: 'subscription-form.html'
})
export class SubscriptionFormPage {
  //libels and holdplaces
  page_title : string;
  title_placeholder : string;
  sum_placeholder : string;
  description_placeholder : string;

  //Info
  title : string ;
  sum : number ;
  description : string;

  //Subscription to update
  subscription : Subscription = new Subscription();

  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public subscriptionService: SubscriptionService, 
              public translateService: TranslateService,
              public toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  _render(){
  	this.translateService.get('NEW_SUBSCRIPTION_TITLE').subscribe( value => { this.page_title = value; });
  	this._renderForm();
  }

  _renderForm(){
    this.translateService.get('T_PLACEHOLDER').subscribe( value => { this.title_placeholder = value; });
	  this.translateService.get('SUM_PLACEHOLDER').subscribe( value => { this.sum_placeholder = value; });
    this.translateService.get('DESCRIPTION_PLACEHOLDER').subscribe( value => { this.description_placeholder = value; });
    this._renderValues();
  }

  _renderValues(){
    if(this.navParams.get('subscription') !== null){
      this.subscription = this.navParams.get('subscription');
      this.title = this.subscription.getTitle();
      this.sum = this.subscription.getSum();
      this.description = this.subscription.getDescription();
    } 
  }

  _loadModel(){
    this.subscription.setTitle(this.title);
    this.subscription.setSum(this.sum);
    this.subscription.setDescription(this.description);
  }

  save(){
    let that = this;
    this._loadModel();
    
    this.subscriptionService.save(this.subscription)
      .then( () => { 
        that.translateService.get('SUBSCRIPTION_CREATED').subscribe( value => { that._lunchToast(value) });
      })
      .catch( (err) => { console.error('Errooor', err); });
    this.navCtrl.pop(this);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
