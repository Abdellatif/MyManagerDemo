/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController} from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Transfer } from '../../models/Transfer';

/********************* Service ************************/
import { TransferService } from '../../services/TransferService';

@Component({
  selector: 'page-transfer-form',
  templateUrl: 'transfer-form.html'
})
export class TransferFormPage {

  //libels and holdplaces
  page_title : string;
  title_placeholder : string;
  sum_placeholder : string;
  description_placeholder : string;


  //Info
  title : string ;
  sum : number ;
  description : string;
  income : boolean
  
  // Transfer to update
  transfer : Transfer = new Transfer();

  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public transferService: TransferService, 
              public translateService: TranslateService,
              public toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  _render(){
  	this.translateService.get('NEW_TRANSFER_TITLE').subscribe( value => { this.page_title = value; });
  	this._renderForm();
  }

  _renderForm(){
    this.translateService.get('T_PLACEHOLDER').subscribe( value => { this.title_placeholder = value; });
	this.translateService.get('SUM_PLACEHOLDER').subscribe( value => { this.sum_placeholder = value; });
    this.translateService.get('DESCRIPTION_PLACEHOLDER').subscribe( value => { this.description_placeholder = value; });
    this._renderValues();
  }

  _renderValues(){
    if(this.navParams.get('transfer') !== null){
      this.transfer = this.navParams.get('transfer');
      this.title = this.transfer.getTitle();
      this.sum = this.transfer.getSum();
      this.description = this.transfer.getDescription();
      this.income = this.transfer.getIncome();
    } 
  }

  _loadModel(){
    this.transfer.setTitle(this.title);
    this.transfer.setSum(this.sum);
    this.transfer.setDescription(this.description);
    this.transfer.setIncome(this.income);
  }

  save(){
    this._loadModel();
    this.transferService.save(this.transfer)
      .then( () => { 
        this.translateService.get('TRANSFER_CREATED').subscribe( value => { this._lunchToast(value) });
      })
      .catch( (err) => { console.error('Errooor', err); });
    this.navCtrl.pop(this);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
