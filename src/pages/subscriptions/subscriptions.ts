/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, ActionSheetController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Subscription } from '../../models/Subscription';

<<<<<<< HEAD
/********************* Service ************************/
import { SubscriptionService } from '../../services/SubscriptionService';
=======
/********************* Services ************************/
import { SubscriptionService } from '../../services/SubscriptionService';

/********************* Pages ************************/
import { SubscriptionFormPage } from '../subscription-form/subscription-form';
import { TransfersPage } from '../transfers/transfers';
>>>>>>> global


@Component({
  selector: 'page-subscriptions',
  templateUrl: 'subscriptions.html'
})
export class SubscriptionsPage {
  page_title : string;
  checkboxShow : boolean  = false;
  subscriptions : Array<Subscription>;
  checkedSusbcriptions : Array<Subscription>;

  constructor(public navCtrl: NavController, 
  			      public navParams: NavParams,
  			      public alertCtl : AlertController, 
              public subscriptionService: SubscriptionService,
              public translateService: TranslateService,
              public toastCtrl : ToastController,
              public actionSheetCtrl: ActionSheetController ) {

  }

  ionViewDidLoad() {
  }


  ionViewWillEnter(){
    this._render();
  }

  _render(){
    this.translateService.get('SUBSCRIPTIONS_TITLE').subscribe( value => { this.page_title = value; });
  	this._renderList();
  }

  _renderList(){
    this.subscriptions= [];
  	this.checkedSusbcriptions= [];
    this.checkboxShow = false;
    this.subscriptionService.fetchAll()
      .then( (data) => {  
        for(let i = 0; i < data.data.length; i++) {
          this.subscriptions.push(data.data[i]);
        } 
      })
      .catch( (err) => { console.error('Errooor', err.error); });
  }


  createOrUpdate(subscription : Subscription = null){
    this.checkboxShow = false;
    this.navCtrl.push(SubscriptionFormPage, {subscription : subscription});
  }

  _deleteSelectedItems(){
    for(let i = 0 ; i<this.checkedSusbcriptions.length; i++){
      this.subscriptionService.deleteById(this.checkedSusbcriptions[i].getId())
      .then( () => { 
            this.translateService.get('SUBSCRIPTION_DELETED').subscribe( value => { this._lunchToast(value) });   
       })
      .catch( (err) => { console.error('Errooor', err.error); });
    }
    this._renderList();
  }

  deleteConfirmPrompt() {
    let prompt_title,
        prompt_message,
        confirm_button_libele,
        cancel_button_libele : string ;

    let params = {value1 : this.checkedSusbcriptions.length};

    this.translateService.get('CONFIRM_DELETE_MESSAGE').subscribe( value => { prompt_message = value; });
    this.translateService.get('CONFIRM_DELETE_TITLE').subscribe( value => { prompt_title = value; });
    this.translateService.get('SAVE', params).subscribe( value => { confirm_button_libele = value; });
    this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; });

    let prompt = this.alertCtl.create({
      title: prompt_title,
      message: prompt_message,
      buttons: [
        {
          text: cancel_button_libele,
          role: 'cancel',
          handler: () => {
            this.cancel();
          }
        },
        {
          text: confirm_button_libele,
          handler: () => {
            this._deleteSelectedItems();
          }
        }
      ]
    });
    prompt.present();
  }

  pressEvent(e, subscription : Subscription) {
      try{
        this.checkboxShow = true;    
        this.onChange(e, subscription);
      }catch(err){
        console.error('Errooor', err.error);
      }
  }

  onChange(e, subscription : Subscription){
    let index = this.checkedSusbcriptions.indexOf(subscription);
    index > -1 ? this.checkedSusbcriptions.splice(index, 1) : this.checkedSusbcriptions.push(subscription);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  checkItem(subscription : Subscription) : boolean{
    let index = this.checkedSusbcriptions.indexOf(subscription);
    return index !== -1 ? true: false;
  }

  cancel(){
    this.checkedSusbcriptions = [];
    this.checkboxShow = false;
  }

  selectAll(){
    if(this.checkedSusbcriptions.length === this.subscriptions.length)
      this.checkedSusbcriptions = [];
    else
      this.checkedSusbcriptions = this.subscriptions;
  }


/*
  createActionSheet(subscription : Subscription) {
    let select_libele,
        update_libele,
        delete_libele, 
        cancel_libele : string;

    this.translateService.get('SELECT').subscribe( value => { select_libele = value; });
    this.translateService.get('UPDATE').subscribe( value => { update_libele = value; });
    this.translateService.get('DELETE').subscribe( value => { delete_libele = value; });
    this.translateService.get('CANCEL').subscribe( value => { cancel_libele = value; });

    let actionSheet = this.actionSheetCtrl.create({
      title: subscription.getTitle(),
      cssClass: 'page-subscriptions',
      buttons: [
        {
          text: select_libele,
          role: 'destructive',
          icon: 'checkbox',
          handler: () => {
            this.checkedSusbcriptions.push(subscription);
            this.checkboxShow = true;
          }
        },{
          text: update_libele,
          role : 'update',
          icon: 'brush',
          handler: () => {
            this.createOrUpdateSubscription(subscription);
          }
        },{
          text: delete_libele,
          role : 'delete',
          icon: 'trash',
          handler: () => {
            this.deleteSubscription(subscription.getId());
          }
        },{
          text: cancel_libele,
          role: 'cancel',
          icon: 'close',
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

  */
}

