/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Transfer } from '../../models/Transfer';

/********************* Services ************************/
import { TransferService } from '../../services/TransferService';

/********************* Pages ************************/
import { TransferFormPage } from '../transfer-form/transfer-form';


@Component({
  selector: 'page-transfers',
  templateUrl: 'transfers.html'
})
export class TransfersPage {

  page_title : string;
  checkboxShow : boolean  = false;
  transfers : Array<Transfer>;
  checkedTransfers : Array<Transfer>;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtl : AlertController, 
              public transferService: TransferService,
              public translateService: TranslateService,
              public toastCtrl : ToastController ) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  _render(){
    this.translateService.get('TRANSFERS_TITLE').subscribe( value => { this.page_title = value; });
    this._renderList();
  }

  _renderList(){
    this.transfers= [];
    this.checkedTransfers= [];
    this.checkboxShow = false;
    this.transferService.fetchAll()
      .then( (data) => {  
        for(let i = 0; i < data.data.length; i++) {
          this.transfers.push(data.data[i]);
        } 
      })
      .catch( (err) => { console.error('Errooor', err.error); });
  }

  createOrUpdate(transfer : Transfer = null){
    this.checkboxShow = false;
    this.navCtrl.push(TransferFormPage, {transfer : transfer});
  }

  _deleteSelectedItems(){
    for(let i = 0 ; i<this.checkedTransfers.length; i++){
      this.transferService.deleteById(this.checkedTransfers[i].getId())
      .then( () => { 
            this.translateService.get('TRANSFER_DELETED').subscribe( value => { this._lunchToast(value) });   
       })
      .catch( (err) => { console.error('Errooor', err.error); });
    }
    this._renderList();
  }

  deleteConfirmPrompt() {
    let prompt_title,
        prompt_message,
        confirm_button_libele,
        cancel_button_libele : string ;

    let params = {value1 : this.checkedTransfers.length};

    this.translateService.get('CONFIRM_DELETE_MESSAGE').subscribe( value => { prompt_message = value; });
    this.translateService.get('CONFIRM_DELETE_TITLE').subscribe( value => { prompt_title = value; });
    this.translateService.get('SAVE', params).subscribe( value => { confirm_button_libele = value; });
    this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; });

    let prompt = this.alertCtl.create({
      title: prompt_title,
      message: prompt_message,
      buttons: [
        {
          text: cancel_button_libele,
          role: 'cancel',
          handler: () => {
            this.cancel();
          }
        },
        {
          text: confirm_button_libele,
          handler: () => {
            this._deleteSelectedItems();
          }
        }
      ]
    });
    prompt.present();
  }

  pressEvent(e, transfer : Transfer) {
      try{
        this.checkboxShow = true;    
        this.onChange(e, transfer);
      }catch(err){
        console.error('Errooor', err.error);
      }
  }

  onChange(e, transfer : Transfer){
    let index = this.checkedTransfers.indexOf(transfer);
    index > -1 ? this.checkedTransfers.splice(index, 1) : this.checkedTransfers.push(transfer);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  checkItem(transfer : Transfer) : boolean{
    let index = this.checkedTransfers.indexOf(transfer);
    return index !== -1 ? true: false;
  }

  cancel(){
    this.checkedTransfers = [];
    this.checkboxShow = false;
  }

  selectAll(){
    if(this.checkedTransfers.length === this.transfers.length)
      this.checkedTransfers = [];
    else
      this.checkedTransfers = this.transfers;
  }

}
