/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Credit } from '../../models/Credit';

/********************* Services ************************/
import { CreditService } from '../../services/CreditService';

/********************* Pages ************************/
import { CreditFormPage } from '../credit-form/credit-form';


@Component({
  selector: 'page-credits',
  templateUrl: 'credits.html'
})
export class CreditsPage {

  page_title : string;
  checkboxShow : boolean  = false;
  credits : Array<Credit>;
  checkedCredits : Array<Credit>;

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public alertCtl : AlertController, 
              public creditService: CreditService,
              public translateService: TranslateService,
              public toastCtrl : ToastController ) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  _render(){
    this.translateService.get('CREDITS_TITLE').subscribe( value => { this.page_title = value; });
  	this._renderList();
  }

  _renderList(){
    this.credits= [];
  	this.checkedCredits= [];
    this.checkboxShow = false;
    this.creditService.fetchAll()
      .then( (data) => {  
        for(let i = 0; i < data.data.length; i++) {
          this.credits.push(data.data[i]);
        } 
      })
      .catch( (err) => { console.error('Errooor', err.error); });
  }

  createOrUpdate(credit : Credit = null){
    this.checkboxShow = false;
    this.navCtrl.push(CreditFormPage, {credit : credit});
  }

  _deleteSelectedItems(){
    for(let i = 0 ; i<this.checkedCredits.length; i++){
      this.creditService.deleteById(this.checkedCredits[i].getId())
      .then( () => { 
            this.translateService.get('CREDIT_DELETED').subscribe( value => { this._lunchToast(value) });   
       })
      .catch( (err) => { console.error('Errooor', err.error); });
    }
    this._renderList();
  }

  deleteConfirmPrompt() {
    let prompt_title,
        prompt_message,
        confirm_button_libele,
        cancel_button_libele : string ;

    let params = {value1 : this.checkedCredits.length};

    this.translateService.get('CONFIRM_DELETE_MESSAGE').subscribe( value => { prompt_message = value; });
    this.translateService.get('CONFIRM_DELETE_TITLE').subscribe( value => { prompt_title = value; });
    this.translateService.get('SAVE', params).subscribe( value => { confirm_button_libele = value; });
    this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; });

    let prompt = this.alertCtl.create({
      title: prompt_title,
      message: prompt_message,
      buttons: [
        {
          text: cancel_button_libele,
          role: 'cancel',
          handler: () => {
            this.cancel();
          }
        },
        {
          text: confirm_button_libele,
          handler: () => {
            this._deleteSelectedItems();
          }
        }
      ]
    });
    prompt.present();
  }

  pressEvent(e, credit : Credit) {
      try{
        this.checkboxShow = true;    
        this.onChange(e, credit);
      }catch(err){
        console.error('Errooor', err.error);
      }
  }

  onChange(e, credit : Credit){
    let index = this.checkedCredits.indexOf(credit);
    index > -1 ? this.checkedCredits.splice(index, 1) : this.checkedCredits.push(credit);
  }

  _lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  checkItem(credit : Credit) : boolean{
    let index = this.checkedCredits.indexOf(credit);
    return index !== -1 ? true: false;
  }

  cancel(){
    this.checkedCredits = [];
    this.checkboxShow = false;
  }

  selectAll(){
    if(this.checkedCredits.length === this.credits.length)
      this.checkedCredits = [];
    else
      this.checkedCredits = this.credits;
  }

}
