/********************* Modules ************************/
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController} from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/********************* Modeles ************************/
import { Subscription } from '../../models/Subscription';
import { Objectif } from '../../models/Objectif';
import { Month } from '../../models/Month';
import { Credit } from '../../models/Credit';

/********************* Service ************************/
import { SubscriptionService } from '../../services/SubscriptionService';
import { CreditService } from '../../services/CreditService';
import { MonthService } from '../../services/MonthService';
import { ObjectifService } from '../../services/ObjectifService';


@Component({
  selector: 'page-new-objectif',
  templateUrl: 'new-objectif.html'
})
export class NewObjectifPage {
  //libels and holdplaces
  page_title : string;
  sum_placeholder : string;
  period_placeholder : string;
  description_placeholder : string;
  subscriptions_title : string;
  credits_title : string;
  
  //General Info
  sum : number ;
  period : number ;
  description : string;
  
  isSumSaved : boolean = false;
  isPeriodSaved : boolean = false;
  
  //Credis and subscriptions
  subscriptions : Array<Subscription> = new Array();
  linkedSubscriptions : Array<number> = new Array();
  credits : Array<Credit> = new Array();
  linkedCredits : Array<number> = new Array();

  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public alertCtl : AlertController, 
              public subscriptionService: SubscriptionService, 
              public creditService: CreditService, 
              public objectifService: ObjectifService, 
              public monthService: MonthService, 
              public translateService: TranslateService,
              public toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this._render();
  }

  //refresh the whole page
  _render(){
    this.translateService.get('NEW_OBJECTIF_TITLE').subscribe( value => { this.page_title = value; });
    this.linkedSubscriptions= [];
    this.linkedCredits= [];

    this._renderGeneralInfo();
    this._renderSubscriptions();
    this._renderCredits();
  }
  
  /******************************************************************************************/
  /************************* General informations ******************************************/
  /****************************************************************************************/

  // refresh the informations section
  _renderGeneralInfo(){
    this.translateService.get('SUM_PLACEHOLDER').subscribe( value => { this.sum_placeholder = value; });
    this.translateService.get('PERIOD_PLACEHOLDER').subscribe( value => { this.period_placeholder = value; });
    this.translateService.get('DESCRIPTION_PLACEHOLDER').subscribe( value => { this.description_placeholder = value; });
    this.translateService.get('SUBSCRIPTIONS_TITLE').subscribe( value => { this.subscriptions_title = value; });
    this.translateService.get('CREDITS_TITLE').subscribe( value => { this.credits_title = value; });
  }

  // disable the sum input
  saveSum(){
    this.isSumSaved = true;
  }

  // enable the sum input
  updateSum(){
    this.isSumSaved = false;
  }
  
  // disable the periode input
  savePeriod(){
    this.isPeriodSaved = true;
  }

  // enable the periode input
  updatePeriod(){
    this.isPeriodSaved = false; 
  }
  
  /******************************************************************************************/
  /************************* Susbcriptions section *****************************************/
  /****************************************************************************************/

  //refresh the subscriptions table
  _renderSubscriptions(){
    this.subscriptions = [];
    for(let i = 0; i<this.linkedSubscriptions.length; i++){
      this.subscriptionService.getById(this.linkedSubscriptions[i])
        .then( (data) => {
          this.subscriptions.push(data.data);
        })
        .catch( (err) => { console.error('Errooor', err.error); }); 
    }
  }

  //Alert to choose the subscriptions
  subscriptionsPrompt(){
    this.subscriptionService.fetchAll()
      .then( (data) => { 
        let prompt_title,
            save_button_libele,
            cancel_button_libele : string ;

        this.translateService.get('CHOOSE_SUBSCRIPTIONS').subscribe( value => { prompt_title = value; });
        this.translateService.get('SAVE').subscribe( value => { save_button_libele = value; });
        this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; }); 
        
        let prompt = this.alertCtl.create();
        prompt.setTitle(prompt_title);    
        for(let i = 0; i < data.data.length; i++) {
          prompt.addInput({
            type : 'checkbox',
            label : data.data[i].getTitle(),
            value : data.data[i].getId(),
            checked: this.existInTable(data.data[i].getId(), this.linkedSubscriptions)
          });
        }
        prompt.addButton(cancel_button_libele);
        prompt.addButton({
          text: save_button_libele,
          handler: data => {
            this.linkedSubscriptions = data;
            this._renderSubscriptions();
          }
        });
        prompt.present(); 
      })
      .catch( (err) => { console.error('Errooor', err.error); });
  }

  /******************************************************************************************/
  /************************* Credits section ***********************************************/
  /****************************************************************************************/

  //refresh the credits table
  _renderCredits(){
    //get all credits
    this.credits = [];
    for(let i = 0; i<this.linkedCredits.length; i++){
      this.creditService.getById(this.linkedCredits[i])
        .then( (data) => {
          this.credits.push(data.data);
        })
        .catch( (err) => { console.error('Errooor', err.error); }); 
    }

  }

  //Alert to choose the credits
  creditsPrompt(){
    this.creditService.fetchAll()
      .then( (data) => { 
        let prompt_title,
            save_button_libele,
            cancel_button_libele : string ;

        this.translateService.get('CHOOSE_CREDITS').subscribe( value => { prompt_title = value; });
        this.translateService.get('SAVE').subscribe( value => { save_button_libele = value; });
        this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; }); 
        
        let prompt = this.alertCtl.create();
        prompt.setTitle(prompt_title);    
        for(let i = 0; i < data.data.length; i++) {
          prompt.addInput({
            type : 'checkbox',
            label : data.data[i].getTitle(),
            value : data.data[i].getId(),
            checked: this.existInTable(data.data[i].getId(), this.linkedCredits)
          });
        }
        prompt.addButton(cancel_button_libele);
        prompt.addButton({
          text: save_button_libele,
          handler: data => {
            this.linkedCredits = data;
            this._renderCredits();
          }
        });
        prompt.present(); 
      })
      .catch( (err) => { console.error('Errooor', err.error); });
  }

  /*
  creditPrompt(credit : Credit = null) {
    let prompt_title, 
        prompt_message, 
        sum_placeholder, 
        title_placeholder, 
        description_placeholder,
        endYear_placeholder,
        endMonth_placeholder,
        save_button_libele,
        cancel_button_libele : string ;

    this.translateService.get('NEW_CREDIT_TITLE').subscribe( value => { prompt_message = value; });
    this.translateService.get('NEW_CREDIT_TITLE').subscribe( value => { prompt_title = value; });
    this.translateService.get('SUM_PLACEHOLDER').subscribe( value => { sum_placeholder = value; });
    this.translateService.get('DESCRIPTION_PLACEHOLDER').subscribe( value => { description_placeholder = value; });
    this.translateService.get('T_PLACEHOLDER').subscribe( value => { title_placeholder = value; });
    this.translateService.get('END_YEAR_PLACEHOLDER').subscribe( value => { endYear_placeholder = value; });
    this.translateService.get('END_MONTH_PLACEHOLDER').subscribe( value => { endMonth_placeholder = value; });
    this.translateService.get('SAVE').subscribe( value => { save_button_libele = value; });
    this.translateService.get('CANCEL').subscribe( value => { cancel_button_libele = value; });

    let prompt = this.alertCtl.create({
      title: prompt_title,
      message: prompt_message,
      inputs: [
        {
          name: 'title',
          placeholder: title_placeholder,
          value : (credit == null) ? "" : credit.getTitle()
        },
        {
          name: 'sum',
          placeholder: sum_placeholder,
          value : (credit == null) ? "" : credit.getTitle()
        },
        {
          name:'description',
          placeholder:description_placeholder,
          value : (credit == null) ? "" : credit.getDescription()  
        },
        {
          name:'endMonth',
          type:'month',
          placeholder:endMonth_placeholder,
          value : (credit == null) ? "" : credit.getTitle()  
        },
        {
          name:'endYear',
          type:'month',
          placeholder:endYear_placeholder,
          value : (credit == null) ? "" : credit.getTitle()   
        },
      ],
      buttons: [
        {
          text: cancel_button_libele,
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: save_button_libele,
          handler: data => {
            let cr = new Credit(data.title, data.sum, data.description, data.endMonth, data.endYear);
            if(credit != null)
              cr.setId(credit.getId());
            this.saveCredit(cr);
            this._renderCredits();

          }
        }
      ]
    });
    prompt.present();
  }
	

  //save credit
  saveCredit(credit : Credit){
    this.creditService.save(credit)
      .then( () => { 
            this.translateService.get('CREDIT_CREATED').subscribe( value => { this.lunchToast(value) });   
       })
      .catch( (err) => { console.error('Errooor', err.error); });  
  }

  //delete credit
  deleteCredit(id : number){
    this.creditService.deleteById(id)
      .then( () => { 
            this.translateService.get('CREDIT_DELETED').subscribe( value => { this.lunchToast(value) });   
       })
      .catch( (err) => { console.error('Error', err.error)});
    this._renderCredits();
  }
  */

  /******************************************************************************************/
  /************************* save method ************************************************/
  /****************************************************************************************/

  saveObjectif(){
    this.objectifService.save(new Objectif(this.period, this.sum, this.description, false))
      .then( (data) => {
        this.translateService.get('OBJECTIF_CREATED').subscribe( value => { this.lunchToast(value) });   
        this.saveMonth(new Month(4, 2017, 2000, null, data.data.getId()));
      })
      .catch( (err) => { console.error('Errooor : ', err.error); });   
  }

  saveMonth(month : Month){
    // Create the month
    this.monthService.save(month)
      .then( (data) => {
        this.lunchToast("Mois ajoutéééé");  
        // Attache the month with the subscriptions
        for(let i = 0; i<this.linkedSubscriptions.length; i++){
          this.monthService.saveAssociatedSusbcription(data.data.getId(), this.linkedSubscriptions[i])
            .then( (data) => { 
                 this.lunchToast("liaison month sub ajoutéééé");   
            })
            .catch( (err) => { console.error('Errooor save associated Susbcription', err.error); });
        }
        
        //Attach the month with the credits
        
        for(let i = 0; i<this.linkedCredits.length; i++){
          this.monthService.saveAssociatedCredit(data.data.getId(), this.linkedCredits[i])
            .then( () => { 
                 this.lunchToast("liaison month credit ajoutéééé");   
            })
            .catch( (err) => { console.error('Errooor save associated Credit', err.error); });
        }
              
      })
      .catch( (err) => { console.error('Errooor save month', err.error.err); });
  }

  /******************************************************************************************/
  /************************* common methods ************************************************/
  /****************************************************************************************/

  // lunch a toast with a message
  lunchToast(message : string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  // check if an element exist in table 
  existInTable(element : any, table:Array<any>) : boolean{
    let index = table.indexOf(element);
    return index !== -1 ? true: false;
  }

  // remove element from the linked table
  remove(element : any){
    if(element instanceof Subscription){
      let index = this.linkedSubscriptions.indexOf(element.getId());
      if(index > -1){
        this.linkedSubscriptions.splice(index, 1);
        this._renderSubscriptions();
      }
    }
    else if(element instanceof Credit){
      let index = this.linkedCredits.indexOf(element.getId());
      if(index > -1){
        this.linkedCredits.splice(index, 1);
        this._renderCredits();
      }
    }

  }


}
