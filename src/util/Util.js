/********************* Models ************************/
import { Credit } from '../models/Credit';
import { Month } from '../models/Month';
import { Objectif } from '../models/Objectif';
import { Subscription } from '../models/Subscription';
import { Transfer } from '../models/Transfer';
import { DailySpent } from '../models/DailySpent';
/********************* Services ************************/
import { CreditService } from '../services/CreditService';
import { MonthService } from '../services/MonthService';
import { ObjectifService } from '../services/ObjectifService';
import { SubscriptionService } from '../services/SubscriptionService';

export class Util{
	// Method to fill the tables of a model
	public static loadModel(model : any){
		if(model instanceof Credit)
			Util.loadCredit(model);
		
		else if(model instanceof Subscription)
			Util.loadSubscription(model);
		
		else if(model instanceof Objectif)
			Util.loadObjectif(model);
		
		else if(model instanceof Month)
			Util.loadMonth(model);
	}

	public static loadCredit(credit : Credit){
		let months = new Array<Month>();
		let creditService = new CreditService();
		creditService.getAssociatedMonths()
		    .then( (data) => {
		      	let monthService = new MonthService();
		        for(let i = 0; i < data.data.length; i++) {
		       		monthService.getById()
		       		.then( (data) => { 
			       		months.push(data.data);     	 
			       	})
			      	.catch( (err) => { console.error('Errooor', err.error); });
		        }
		        credit.setMonths(months); 
		      })
		      .catch( (err) => { console.error('Errooor', err.error); });
	}

	public static loadSubscription(subscription : Subscription){
		let months = new Array<Month>();
		let subscriptionService = new SubscriptionService();
		subscriptionService.getAssociatedMonths()
			.then( (data) => {
		      	let monthService = new MonthService();
		        for(let i = 0; i < data.data.length; i++) {
		       		monthService.getById()
		       		.then( (data) => { 
			       		months.push(data.data);     	 
			       	})
			      	.catch( (err) => { console.error('Errooor', err.error); });
		        }
		        subscription.setMonths(months); 
		      })
		      .catch( (err) => { console.error('Errooor', err.error); });
	}

	public static loadObjectif(objectif : Objectif){
		let months = new Array<Month>();
		let objectifService = new ObjectifService();
		objectifService.getAssociatedMonths()
			.then( (data) => {
				let monthService = new MonthService();
			    for(let i = 0; i < data.data.length; i++) {
			       	monthService.getById()
			       	.then( (data) => { 
				    	months.push(data.data);     	 
				     })
				     .catch( (err) => { console.error('Errooor', err.error); });
			    }
			    objectif.setMonths(months); 
			})
			.catch( (err) => { console.error('Errooor', err.error); });
	}

	public static loadMonth(month : Month){
		let subscriptions = new Array<Subscription>();
		let credits = new Array<Credit>();
		let dailySpents = new Array<DailySpent>();
		let transfers = new Array<Transfer>();
		let monthService = new MonthService();

		monthService.getAssociatedSubscriptions()
			.then( (data) => {
				let subscriptionService = new SubscriptionService();
			    for(let i = 0; i < data.data.length; i++) {
			       	subscriptionService.getById()
			       	.then( (data) => { 
				    	subscriptions.push(data.data);     	 
				     })
				     .catch( (err) => { console.error('Errooor', err.error); });
			    }
			    month.setMonths(subscriptions); 
			})
			.catch( (err) => { console.error('Errooor', err.error); });

		monthService.getAssociatedCredits()
			.then( (data) => {
				let creditService = new CreditService();
			    for(let i = 0; i < data.data.length; i++) {
			       	creditService.getById()
			       	.then( (data) => { 
				    	credits.push(data.data);     	 
				     })
				     .catch( (err) => { console.error('Errooor', err.error); });
			    }
			    month.setMonths(credits); 
			})
			.catch( (err) => { console.error('Errooor', err.error); });

		monthService.getAssociatedDailySpents()
			.then( (data) => {
				let dailySpentService = new DailySpentService();
			    for(let i = 0; i < data.data.length; i++) {
			       	dailySpentService.getById()
			       	.then( (data) => { 
				    	dailySpents.push(data.data);     	 
				     })
				     .catch( (err) => { console.error('Errooor', err.error); });
			    }
			    month.setMonths(dailySpents); 
			})
			.catch( (err) => { console.error('Errooor', err.error); });

		monthService.getAssociatedTransfers()
			.then( (data) => {
				let transferService = new TransferService();
			    for(let i = 0; i < data.data.length; i++) {
			       	transferService.getById()
			       	.then( (data) => { 
				    	transfers.push(data.data);     	 
				     })
				     .catch( (err) => { console.error('Errooor', err.error); });
			    }
			    month.setMonths(transfers); 
			})
			.catch( (err) => { console.error('Errooor', err.error); });
	}
		
}