/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { Month } from '../models/Month'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class MonthService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a Month
    * @param {Month} the Month to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(month : Month ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {

              let sqlRequest = (month.getId() == null) ? "INSERT INTO month (month, year, salary, shortObjectif, objectif_id) VALUES (?, ?, ?, ?, ?)" 
                                                          : "UPDATE month SET month=?, year=?, salary=?, shortObjectif, objectif_id WHERE id=?";
              let params = [month.getMonth(), month.getYear(), month.getSalary(), month.getShortObjectif(), month.getObjectifId()];
              
              if(month.getId() != null)
                params.push(month.getId());

              this.sql.query(sqlRequest, params)
                .then((data) => { 
                    try {
                          this.sql.query("SELECT * FROM month WHERE id=(SELECT MAX(id) FROM month)")
                            .then((data) => {
                                console.log(data.res.rows.item(0));
                                if(data.res.rows.length > 0) {
                                    resolve({ data: new Month(data.res.rows.item(0).month, 
                                                              data.res.rows.item(0).year, 
                                                              data.res.rows.item(0).salary, 
                                                              data.res.rows.item(0).shortObjectif, 
                                                              data.res.rows.item(0).objectif_id,
                                                              data.res.rows.item(0).id)
                                            });  
                                 }
                                 resolve({ data: new Month() });
                            })
                            .catch( (error) => { reject({ error: error }); });
                    } catch (error) {
                            console.log("save Month error");
                            reject({ error: error });
                    }
                })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a month 
    * @param {Month} the month to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(month : Month) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE month SET month=?, year=?, salary=?, shortObjectif, objectif_id WHERE id=?", [month.getMonth(), month.getYear(), month.getSalary(), month.getShortObjectif(), month.getObjectifId(), month.getId()])
                    .then((data) => { //must resolve the updated subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all month from data base
    * @return {Promise} that resolves or reject with an object with the form {data : MonthList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM month")
                .then((data) => { let monthList = new Array<Month>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let month = new Month(data.res.rows.item(i).month, 
                                                              data.res.rows.item(i).year, 
                                                              data.res.rows.item(i).salary, 
                                                              data.res.rows.item(i).shortObjectif, 
                                                              data.res.rows.item(i).objectif_id,
                                                              data.res.rows.item(i).id);
                                                               
                                        monthList.push(month);
                                    }
                                 }
                                 resolve({ data: monthList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a month by its ID from data base
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from month WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Get a month by its ID from data base
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : Month} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM month WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new Month(data.res.rows.item(0).month, 
                                                              data.res.rows.item(0).year, 
                                                              data.res.rows.item(0).salary, 
                                                              data.res.rows.item(0).shortObjectif, 
                                                              data.res.rows.item(0).objectif_id,
                                                              null,
                                                              null, 
                                                              data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new Month() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**********************************************************************************/
    /********************* Subscription Month relationship ***************************/
    /********************************************************************************/

    /**
    * Get a list of the associated subscriptions id of the month by its ID 
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : subscriptionIdList} or {error : Error}
    */
    public getAssociatedSubscriptions(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT subscription_id FROM month_subscription where month_id=?", [id])
                .then((data) => { let subscriptionIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        subscriptionIdList.push(data.res.rows.item(i).subscription_id);
                                    }
                                 }
                                 resolve({ data: subscriptionIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Add or update a month_subscription
    * @param {number} month_id, The month id
    * @param {number} new_subscription_id, the new subscription id
    * @param {number} old_subscription_id, The old subscription id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public saveAssociatedSusbcription(month_id : number, new_subscription_id : number, old_subscription_id : number = null){
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (old_subscription_id == null) ? "INSERT INTO month_subscription (month_id, subscription_id) VALUES (?, ?)" 
                                                               : "UPDATE month_subscription SET subscription_id=? WHERE month_id=? and subscription_id=?";
                let params = [];
                if(old_subscription_id != null){
                    params.push(new_subscription_id);
                    params.push(month_id);
                    params.push(old_subscription_id);
                }
                else{
                  params.push(month_id);
                  params.push(new_subscription_id);
                }
                this.sql.query(sqlRequest, params)
                    .then((data) => { 
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * delete a month_subscription
    * @param {number} subscription_id The subscription id
    * @param {number} Month_id The month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteAssociatedSusbcription(month_id : number, subscription_id : number){
         return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from month_subscription WHERE subscription_id=? and month_id=?", [subscription_id, month_id])
                .then((data) => { 
                    resolve({ data: data }); 
                })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**********************************************************************************/
    /************************** Credit Month relationship ****************************/
    /********************************************************************************/

    /**
    * Get a list of the associated credits id of the month 
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : creditIdList} or {error : Error}
    */
    public getAssociatedCredits(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT credit_id FROM month_credit where month_id=?", [id])
                .then((data) => { let creditIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        creditIdList.push(data.res.rows.item(i).credit_id);
                                    }
                                 }
                                 resolve({ data: creditIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * delete a month_credit
    * @param {number} Month_id The month id
    * @param {number} new_credit_id The new credit id
    * @param {number} old_credit_id The old credit id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public saveAssociatedCredit(month_id : number, new_credit_id : number, old_credit_id : number = null){
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (old_credit_id == null) ? "INSERT INTO month_credit (credit_id, month_id) VALUES (?, ?)" 
                                                               : "UPDATE credit_subscription SET credit_id=? WHERE month_id=? and credit_id=?";
                let params = [new_credit_id, month_id];
                if(old_credit_id != null)
                    params.push(old_credit_id);

                this.sql.query(sqlRequest, params)
                    .then((data) => { 
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * delete a month_credit
    * @param {number} credit_id The credit id
    * @param {number} Month_id The month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteAssociatedCredit(month_id : number, credit_id : number){
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from month_credit WHERE credit_id=? and month_id=?", [credit_id, month_id])
                .then((data) => { 
                    resolve({ data: data }); 
                })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });
    }

    /**********************************************************************************/
    /********************** DailySpent Month relationship ****************************/
    /********************************************************************************/

    /**
    * Get a list of the associated dailyspents_id of the month 
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : dailySpentIdList} or {error : Error}
    */
    public getAssociatedDailySpents(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT id FROM dailyspent where month_id=?", [id])
                .then((data) => { let dailySpentIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        dailySpentIdList.push(data.res.rows.item(i).id);
                                    }
                                 }
                                 resolve({ data: dailySpentIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**********************************************************************************/
    /************************** Transfer Month relationship **************************/
    /********************************************************************************/

    /**
    * Get a list of the associated transfers_id of the month 
    * @param {number} id, the id of the month
    * @return {Promise} that resolves or reject with an object with the form {data : transferIdList} or {error : Error}
    */
    public getAssociatedTransfers(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT id FROM transfer where month_id=?", [id])
                .then((data) => { let transferIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        transferIdList.push(data.res.rows.item(i).id);
                                    }
                                 }
                                 resolve({ data: transferIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

}