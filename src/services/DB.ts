/********************* Providers ************************/
import {Sql} from "../providers/Sql";

export class DB {	
	private static instance: Sql = null;

	private constructor(){
	}

	public static getInstance(): Sql {
    	if (DB.instance === null) {
       		DB.instance = new Sql();
    	}
    	return DB.instance;
  	}
}