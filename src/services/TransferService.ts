/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { Transfer } from '../models/Transfer'
//import { Month } from '../models/Month'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class TransferService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a transfer
    * @param {Transfer} the transfer to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(transfer : Transfer ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {

                let sqlRequest = (transfer.getId() == null) ? "INSERT INTO transfer (title, sum, description, income, month_id) VALUES (?, ?, ?, ?, ?)" 
                                                            : "UPDATE transfer SET title=?, sum=?, description=?, income=?, month_id=? WHERE id=?";
                let params = [transfer.getTitle(), transfer.getSum(), transfer.getDescription(), transfer.getIncome(), transfer.getMonthId()];
                
                if(transfer.getId() != null)
                    params.push(transfer.getId());
                
                this.sql.query(sqlRequest, params)
                    .then((data) => { // must resolve the last transfer
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a transfer 
    * @param {Transfert} the transfer to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(transfer : Transfer) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE transfer SET title=?, sum=?, description=?, income=?, month_id=? WHERE id=?", [transfer.getTitle(), transfer.getSum(), transfer.getDescription(), transfer.getIncome(), transfer.getMonthId(), transfer.getId()])
                    .then((data) => { //must resolve the updated transfer
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all transfers from data base
    * @return {Promise} that resolves or reject with an object with the form {data : TransferList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM transfer")
                .then((data) => { let transferList = new Array<Transfer>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let transfer = new Transfer(data.res.rows.item(i).title, 
                                                                            data.res.rows.item(i).sum, 
                                                                            data.res.rows.item(i).description,
                                                                            data.res.rows.item(i).income,
                                                                            data.res.rows.item(i).month_id, 
                                                                            data.res.rows.item(i).id);
                                        transferList.push(transfer);
                                    }
                                 }
                                 resolve({ data: transferList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a transfer by its ID from data base
    * @param {number} id, the id of the transfer
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from transfer WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Get a transfer by its ID from data base
    * @param {number} id, the id of the transfer
    * @return {Promise} that resolves or reject with an object with the form {data : Transfer} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM transfer WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new Transfer(data.res.rows.item(0).title, 
                                                             data.res.rows.item(0).sum, 
                                                             data.res.rows.item(0).description,
                                                             data.res.rows.item(0).income,
                                                             data.res.rows.item(0).month_id, 
                                                             data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new Transfer() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }
}