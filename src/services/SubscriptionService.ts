/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { Subscription } from '../models/Subscription'
import { Month } from '../models/Month'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class SubscriptionService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a subscription
    * @param {Subscription} the subscription to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(subscription : Subscription ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (subscription.getId() == null) ? "INSERT INTO subscription (title, sum, description) VALUES (?, ?, ?)" 
                                                                : "UPDATE subscription SET title=?, sum=?, description=? WHERE id=?";
                let params = [subscription.getTitle(), subscription.getSum(), subscription.getDescription()];
                
                if(subscription.getId() != null)
                    params.push(subscription.getId());
                
                this.sql.query(sqlRequest, params)
                    .then((data) => { 
                        try {
                            this.sql.query("SELECT * FROM subscription WHERE id=(SELECT MAX(id)  FROM subscription)")
                            .then((data) => {
                                if(data.res.rows.length > 0) {
                                    resolve({ data: new Subscription(data.res.rows.item(0).title, 
                                                                     data.res.rows.item(0).sum, 
                                                                     data.res.rows.item(0).description, 
                                                                     data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new Subscription() }); 
                            })
                            .catch( (error) => { reject({ error: error }); });
                        } catch (error) {
                            reject({ error: error });
                        } 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a subscription 
    * @param {Subscription} the subscription to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(subscription : Subscription) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE subscription SET title=?, sum=?, description=? WHERE id=?", [subscription.getTitle(), subscription.getSum(), subscription.getDescription(), subscription.getId()])
                    .then((data) => { //must resolve the updated subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all subscriptions from data base
    * @return {Promise} that resolves or reject with an object with the form {data : SubscriptionList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM subscription")
                .then((data) => { let subscriptionList = new Array<Subscription>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let subscription = new Subscription(data.res.rows.item(i).title, 
                                                                            data.res.rows.item(i).sum, 
                                                                            data.res.rows.item(i).description, 
                                                                            data.res.rows.item(i).id);
                                        subscriptionList.push(subscription);
                                    }
                                 }
                                 resolve({ data: subscriptionList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a subscription by its ID from data base
    * @param {number} id, the id of the subscription
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from subscription WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Get a subscription by its ID from data base
    * @param {number} id, the id of the subscription
    * @return {Promise} that resolves or reject with an object with the form {data : Subscription} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM subscription WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new Subscription(data.res.rows.item(0).title, 
                                                                     data.res.rows.item(0).sum, 
                                                                     data.res.rows.item(0).description, 
                                                                     data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new Subscription() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**********************************************************************************/
    /********************** Subscription Month relationship **************************/
    /********************************************************************************/

    /**
    * Get a list of the associated months id of the subscription by its ID 
    * @param {number} id, the id of the subscription
    * @return {Promise} that resolves or reject with an object with the form {data : monthIdList} or {error : Error}
    */
    public getAssociatedMonths(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT month_id FROM month_subscription where subscription_id=?", [id])
                .then((data) => { let monthIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        monthIdList.push(data.res.rows.item(i).month_id);
                                    }
                                 }
                                 resolve({ data: monthIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Add or update a month_subscription
    * @param {number} subscription_id, the subscription id
    * @param {number} new_month_id, The new month id
    * @param {number} old_month_id, The old month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public saveAssociatedMonth(subscription_id : number, new_month_id : number, old_month_id : number = null ){
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (old_month_id == null) ? "INSERT INTO month_subscription (month_id, subscription_id) VALUES (?, ?)" 
                                                        : "UPDATE month_subscription SET month_id=? WHERE subscription_id=? and month_id=?";
                let params = [new_month_id, subscription_id];
                if(old_month_id != null)
                    params.push(old_month_id);

                this.sql.query(sqlRequest, params)
                    .then((data) => { 
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * delete a month_subscription
    * @param {number} subscription_id The subscription id
    * @param {number} Month_id The month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteAssociatedMonth(subscription_id : number, month_id : number){
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from month_subscription WHERE subscription_id=? and month_id=?", [subscription_id, month_id])
                .then((data) => { 
                    resolve({ data: data }); 
                })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }
}