/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { Credit } from '../models/Credit'
import { Month } from '../models/Month'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class CreditService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a credit
    * @param {Credit} The credit to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(credit : Credit ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (credit.getId() == null) ? "INSERT INTO credit (title, sum, description, endMonth, endYear) VALUES (?, ?, ?, ?, ?)" 
                                                          : "UPDATE credit SET title=?, sum=?, description=?, endMonth=?, endYear=? WHERE id=?";
                let params = [credit.getTitle(), credit.getSum(), credit.getDescription(), credit.getEndMonth(), credit.getEndYear()];
              
                if(credit.getId() != null)
                    params.push(credit.getId());

                this.sql.query(sqlRequest, params)
                    .then((data) => { // must resolve the last subscription
                        try {
                            this.sql.query("SELECT * FROM credit WHERE id=(SELECT MAX(id)  FROM credit)")
                            .then((data) => {
                                             if(data.res.rows.length > 0) {
                                                resolve({ data: new Credit( data.res.rows.item(0).title,
                                                                            data.res.rows.item(0).sum,
                                                                            data.res.rows.item(0).description, 
                                                                            data.res.rows.item(0).endMonth, 
                                                                            data.res.rows.item(0).endYear, 
                                                                            data.res.rows.item(0).id)
                                                        });
                                             }
                                             resolve({ data: new Credit() }); 
                                           })
                            .catch( (error) => { reject({ error: error }); });
                        } catch (error) {
                            reject({ error: error });
                        }
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a credit 
    * @param {Credit} The credit to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(credit : Credit) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE credit SET title=?, sum=?, description=?, endMonth=?, endYear=? WHERE id=?", [credit.getTitle(), credit.getSum(), credit.getDescription(), credit.getEndMonth(), credit.getEndYear(), credit.getId()])
                    .then((data) => { //must resolve the updated subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all credits from data base
    * @return {Promise} that resolves or reject with an object with the form {data : CreditList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM credit")
                .then((data) => { let creditList = new Array<Credit>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let credit = new Credit(data.res.rows.item(i).title,
                                                                data.res.rows.item(i).sum,
                                                                data.res.rows.item(i).description, 
                                                                data.res.rows.item(i).endMonth, 
                                                                data.res.rows.item(i).endYear,
                                                                data.res.rows.item(i).id);
                                        creditList.push(credit);
                                    }
                                 }
                                 resolve({ data: creditList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a credit by its ID from data base
    * @param {number} id, the id of the credit
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from credit WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Get a credit by its ID from data base
    * @param {number} id, the id of the credit
    * @return {Promise} that resolves or reject with an object with the form {data : Credit} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM credit WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new Credit( data.res.rows.item(0).title,
                                                                data.res.rows.item(0).sum,
                                                                data.res.rows.item(0).description, 
                                                                data.res.rows.item(0).endMonth, 
                                                                data.res.rows.item(0).endYear, 
                                                                data.res.rows.item(0).id)
                                            });
                                 }
                                 resolve({ data: new Credit() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**********************************************************************************/
    /************************** Credit Month relationship ****************************/
    /********************************************************************************/

    /**
    * Get a list of the associated months_id of the credit by its ID 
    * @param {number} id, the id of the credit
    * @return {Promise} that resolves or reject with an object with the form {data : monthIdList} or {error : Error}
    */
    public getAssociatedMonths(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT month_id FROM month_credit where credit_id=?", [id])
                .then((data) => { let monthIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        monthIdList.push(data.res.rows.item(i).month_id);
                                    }
                                 }
                                 resolve({ data: monthIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Add or update a month_credit
    * @param {number} credit_id, the credit id
    * @param {number} new_month_id, The new month id
    * @param {number} old_month_id, The old month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public saveAssociatedMonth(credit_id : number,  new_month_id : number, old_month_id : number = null){
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (old_month_id == null) ? "INSERT INTO month_credit (month_id, credit_id) VALUES (?, ?)" 
                                                        : "UPDATE month_credit SET month_id=? WHERE credit_id=? and month_id=?";
                let params = [new_month_id, credit_id];
                if(old_month_id != null)
                    params.push(old_month_id);

                this.sql.query(sqlRequest, params)
                    .then((data) => { 
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * delete a month_credit
    * @param {number} Credit_id, The credit id
    * @param {number} Month_id, The month id
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteAssociatedMonth(credit_id : number, month_id : number){
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from month_credit WHERE credit_id=? and month_id=?", [credit_id, month_id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });   
    }


}