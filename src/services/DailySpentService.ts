/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { DailySpent } from '../models/DailySpent'
//import { Month } from '../models/Month'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class DailySpentService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a dailySpent 
    * @param {DailySpent} the dailySpent to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(dailySpent : DailySpent ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (dailySpent.getId() == null) ? "INSERT INTO dailySpent (title, sum, description, date, eSpent, restSpent, month_id) VALUES (?, ?, ?, ?, ?, ?, ?)" 
                                                              : "UPDATE dailySpent SET title=?, sum=?, description=?, date=?, eSpent=?, restSpent=?, month_id=? WHERE id=?";
                let params = [dailySpent.getTitle(), dailySpent.getSum(), dailySpent.getDescription(), dailySpent.getDate(), dailySpent.getESpent(), dailySpent.getRestSpent(), dailySpent.getMonthId()];
              
                if(dailySpent.getId() != null)
                    params.push(dailySpent.getId());

                this.sql.query(sqlRequest, params)
                    .then((data) => { // must resolve the last subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a dailySpent 
    * @param {DailySpent} the dailySpent to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(dailySpent : DailySpent) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE dailySpent SET title=?, sum=?, description=?, date=?, eSpent=?, restSpent=?, month_id=? WHERE id=?", [dailySpent.getTitle(), dailySpent.getSum(), dailySpent.getDescription(), dailySpent.getDate(), dailySpent.getESpent(), dailySpent.getRestSpent(), dailySpent.getMonthId(), dailySpent.getId()])
                    .then((data) => { //must resolve the updated subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all dailySpents from data base
    * @return {Promise} that resolves or reject with an object with the form {data : DailySpentList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM dailySpent")
                .then((data) => { let dailySpentList = new Array<DailySpent>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let dailySpent = new DailySpent(data.res.rows.item(i).title, 
                                                                            data.res.rows.item(i).sum, 
                                                                            data.res.rows.item(i).description, 
                                                                            data.res.rows.item(i).date, 
                                                                            data.res.rows.item(i).eSpent, 
                                                                            data.res.rows.item(i).restSpent, 
                                                                            data.res.rows.item(i).month_id, 
                                                                            data.res.rows.item(i).id);
                                        dailySpentList.push(dailySpent);
                                    }
                                 }
                                 resolve({ data: dailySpentList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a dailySpent by its ID from data base
    * @param {number} id, the id of the dailySpent
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from dailySpent WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Get a dailySpent by its ID from data base
    * @param {number} id, the id of the dailySpent
    * @return {Promise} that resolves or reject with an object with the form {data : dailySpent} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM dailySpent WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new DailySpent(data.res.rows.item(0).title, 
                                                                     data.res.rows.item(0).sum, 
                                                                     data.res.rows.item(0).description,
                                                                     data.res.rows.item(0).date, 
                                                                     data.res.rows.item(0).eSpent, 
                                                                     data.res.rows.item(0).restSpent, 
                                                                     data.res.rows.item(0).month_id, 
                                                                     data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new DailySpent() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

}