/********************* Modules ************************/
import { Injectable } from '@angular/core';

/********************* Models ************************/
import { Objectif } from '../models/Objectif'

/********************* Services ************************/
import { DB } from '../services/DB'

/********************* Providers ************************/
import {Sql} from "../providers/Sql";

@Injectable()
export class ObjectifService {
    public sql: Sql;

    public constructor() {
        //Get the sql instance
        this.sql = DB.getInstance();
    }

    /**
    * Add or update a objectif 
    * @param {Objectif} the objectif to save
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public save(objectif : Objectif ) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                let sqlRequest = (objectif.getId() == null) ? "INSERT INTO objectif (period, sum, description, isDone) VALUES (?, ?, ?, ?)" 
                                                            : "UPDATE objectif SET period=?, sum=?, description=?, isDone=? WHERE id=?";
                let params = [objectif.getPeriod(), objectif.getSum(), objectif.getDescription(), objectif.getStatus()];
                
                if(objectif.getId() != null)
                    params.push(objectif.getId());
                
                this.sql.query(sqlRequest, params)
                    .then((data) => {
                        try {
                            this.sql.query("SELECT * FROM objectif WHERE id=(SELECT MAX(id) FROM objectif)")
                            .then((data) => {
                                             if(data.res.rows.length > 0) {
                                                resolve({ data: new Objectif(data.res.rows.item(0).title, 
                                                                                 data.res.rows.item(0).sum, 
                                                                                 data.res.rows.item(0).description, 
                                                                                 data.res.rows.item(0).isDone, 
                                                                                 data.res.rows.item(0).id) }); 
                                             }
                                             resolve({ data: new Objectif() }); 
                                           })
                            .catch( (error) => { reject({ error: error }); });
                        } catch (error) {
                            reject({ error: error });
                        }
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Update a objectif 
    * @param {Objectif} the objectif to update
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public update(objectif : Objectif) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("UPDATE objectif SET period=?, sum=?, description=?, isDone=? WHERE id=?", [objectif.getPeriod(), objectif.getSum(), objectif.getDescription(), objectif.getStatus(), objectif.getId()])
                    .then((data) => { //must resolve the updated subscription
                        resolve({ data: "" }); 
                    })
                    .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        }); 
    }

    /**
    * Get all objectifs from data base
    * @return {Promise} that resolves or reject with an object with the form {data : ObjectifList} or {error : Error}
    */
    public fetchAll() : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM objectif")
                .then((data) => { let objectifList = new Array<Objectif>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        let objectif = new Objectif(data.res.rows.item(i).period, 
                                                                            data.res.rows.item(i).sum, 
                                                                            data.res.rows.item(i).description, 
                                                                            data.res.rows.item(i).isDone, 
                                                                            data.res.rows.item(i).id);
                                        objectifList.push(objectif);
                                    }
                                 }
                                 resolve({ data: objectifList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**
    * Delete a objectif by its ID from data base
    * @param {number} id, the id of the objectif
    * @return {Promise} that resolves or reject with an object with the form {data : Result} or {error : Error}
    */
    public deleteById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("DELETE from objectif WHERE id=?", [id])
                .then((data) => { 
                                 resolve({ data: data }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }


    /**
    * Get a objectif by its ID from data base
    * @param {number} id, the id of the objectif
    * @return {Promise} that resolves or reject with an object with the form {data : Objectif} or {error : Error}
    */
    public getById(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT * FROM objectif WHERE id=?", [id])
                .then((data) => {
                                 if(data.res.rows.length > 0) {
                                    resolve({ data: new Objectif(data.res.rows.item(0).title, 
                                                                     data.res.rows.item(0).sum, 
                                                                     data.res.rows.item(0).description, 
                                                                     data.res.rows.item(0).isDone, 
                                                                     data.res.rows.item(0).id) }); 
                                 }
                                 resolve({ data: new Objectif() }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }

    /**********************************************************************************/
    /************************ Objectif Month relationship ****************************/
    /********************************************************************************/

    /**
    * Get a list of the associated months_id of the objectif by its ID 
    * @param {number} id, the id of the objectif
    * @return {Promise} that resolves or reject with an object with the form {data : monthIdList} or {error : Error}
    */
    public getAssociatedMonths(id: number) : Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                this.sql.query("SELECT id FROM month where objectif_id=?", [id])
                .then((data) => { let monthIdList = new Array<number>();
                                 if(data.res.rows.length > 0) {
                                    for(let i = 0; i < data.res.rows.length; i++) {
                                        monthIdList.push(data.res.rows.item(i).id);
                                    }
                                 }
                                 resolve({ data: monthIdList }); 
                               })
                .catch( (error) => { reject({ error: error }); });
            } catch (error) {
                reject({ error: error });
            }
        });        
    }
}