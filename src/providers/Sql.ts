/********************* Modules ************************/
import {Injectable} from "@angular/core";
import { SQLite } from 'ionic-native';
import { Platform } from 'ionic-angular';

/********************* Settings ************************/
import { AppSettings} from '../settings/AppSettings'

const win: any = window;

@Injectable()
export class Sql {
    private _db: any;

    constructor() {
        if (win.sqlitePlugin) {
            this._db = win.sqlitePlugin.openDatabase({
                name: AppSettings.DB_NAME,
                location: AppSettings.LOCATION,
                createFromLocation: 0
            });

        } else {
            console.warn('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');

            this._db = win.openDatabase(AppSettings.DB_NAME, '1.0', 'database', 5 * 1024 * 1024);
        }
        //this._tryDrop();
        this._tryInit();
    }

    /*
    // Initialize the DB with our required tables
    _tryDrop() {
                //execute table monthSubscriptionTable
        this.query('DROP TABLE month_subscription').catch(err => {
            console.error('Storage: Unable to drop month_subscription', err.tx, err.err);
        });

        //execute table monthDailySpentTable
        this.query('DROP TABLE month_dailySpent').catch(err => {
            console.error('Storage: Unable to drop month_dailySpent', err.tx, err.err);
        });

        //execute table monthCreditTable
        this.query('DROP TABLE month_credit').catch(err => {
            console.error('Storage: Unable to drop month_credit', err.tx, err.err);
        });

        //execute table subscriptionTable
        this.query('DROP TABLE subscription').catch(err => {
            console.error('Storage: Unable to drop subscription', err.tx, err.err);
        });

        //execute table dailSpentTable
        this.query('DROP TABLE dailySpent').catch(err => {
            console.error('Storage: Unable to  to drop dailyspent', err.tx, err.err);
        });

        //execute table creditTable
        this.query('DROP TABLE credit').catch(err => {
            console.error('Storage: Unable to drop credit', err.tx, err.err);
        });


        //execute table month
        this.query('DROP TABLE month').catch(err => {
            console.error('Storage: Unable to drop month', err.tx, err.err);
        });


        ///execute table objectif
        this.query('DROP TABLE objectif').catch(err => {
            console.error('Storage: Unable to drop table objectif', err.tx, err.err);
        });
    }
    */
    

    // Initialize the DB with our required tables
    _tryInit() {
        ///execute table objectif
        this.query('CREATE TABLE IF NOT EXISTS objectif (id INTEGER PRIMARY KEY AUTOINCREMENT, sum REAL, period INTEGER, description TEXT, isDone INTEGER)').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table month
        this.query('CREATE TABLE IF NOT EXISTS month (id INTEGER PRIMARY KEY AUTOINCREMENT, month INTEGER, year INTEGER, salary REAL, shortObjectif REAL, objectif_id INTEGER, FOREIGN KEY(objectif_id) REFERENCES objectif(id))').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table subscriptionTable
        this.query('CREATE TABLE IF NOT EXISTS subscription (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, sum REAL, description TEXT)').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table dailSpentTable
        this.query('CREATE TABLE IF NOT EXISTS dailySpent (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, sum REAL, description TEXT, date TEXT, eSpent REAL, restSpent REAL, month_id INTEGER, FOREIGN KEY(month_id) REFERENCES month(id))').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table trasnfer
        this.query('CREATE TABLE IF NOT EXISTS transfer (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, sum REAL, description TEXT, income INTEGER, month_id INTEGER, FOREIGN KEY(month_id) REFERENCES month(id))').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table creditTable
        this.query('CREATE TABLE IF NOT EXISTS credit (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, sum REAL, description TEXT, endYear INTEGER, endMonth INTEGER)').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table monthSubscriptionTable
        this.query('CREATE TABLE IF NOT EXISTS month_subscription (month_id, subscription_id, PRIMARY KEY(month_id, subscription_id), FOREIGN KEY(month_id) REFERENCES month(id), FOREIGN KEY(subscription_id) REFERENCES subscription(id))').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        //execute table monthCreditTable
        this.query('CREATE TABLE IF NOT EXISTS month_credit (month_id, credit_id, PRIMARY KEY(month_id, credit_id), FOREIGN KEY(month_id) REFERENCES month(id), FOREIGN KEY(credit_id) REFERENCES credit(id))').catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

    }

    /**
     * Perform an arbitrary SQL operation on the database. Use this method
     * to have full control over the underlying database through SQL operations
     * like SELECT, INSERT, and UPDATE.
     *
     * @param {string} query the query to run
     * @param {array} params the additional params to use for query placeholders
     * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
     */
    query(query: string, params: any[] = []): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                        tx.executeSql(query, params,
                            //(tx: any, res: any) => resolve({ tx: tx, res: res }),
                            (tx: any, res: any) => resolve({  tx: tx, res: res }),
                            (tx: any, err: any) => reject({ tx: tx, err: err }));
                    },
                    (err: any) => reject({ err: err }));
            } catch (err) {
                reject({ err: err });
            }
        });
    }

    /**
     * Get the value in the database identified by the given key.
     * @param {string} key the key
     * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
     */
    get(key: string): Promise<any> {
        return this.query('select key, value from kv where key = ? limit 1', [key]).then(data => {
            if (data.res.rows.length > 0) {
                return data.res.rows.item(0).value;
            }
        });
    }

    /**
     * Set the value in the database for the given key. Existing values will be overwritten.
     * @param {string} key the key
     * @param {string} value The value (as a string)
     * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
     */
    set(key: string, value: string): Promise<any> {
        return this.query('insert or replace into kv(key, value) values (?, ?)', [key, value]);
    }

    /**
     * Remove the value in the database for the given key.
     * @param {string} key the key
     * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
     */
    remove(key: string): Promise<any> {
        return this.query('delete from kv where key = ?', [key]);
    }

    /**
     * Clear all keys/values of your database.
     * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
     */
    clear(): Promise<any> {
        return this.query('delete from kv');
    }
}