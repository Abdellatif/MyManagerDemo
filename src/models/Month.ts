import { Subscription } from './Subscription'
import { Credit } from './Credit'
import { Transfer } from './Transfer'
import { DailySpent } from './DailySpent'

export  class Month{
	protected id : number;
	protected month : number;
    protected year : number;
    protected salary : number;
    protected shortObjectif : number; 
    protected objectif_id : number;

    protected subscriptions : Array<Subscription>; 
    protected credits : Array<Credit>; 
    protected dailySpents : Array<DailySpent>; 
    protected transfers : Array<Transfer>; 

    public constructor(month: number = null,
    				   year: number = null,
    				   salary: number = null,
    				   shortObjectif: number = null,
    				   objectif_id: number = null,
                       id: number = null,
                       subscriptions: Array<Subscription> = null,
                       credits: Array<Credit> = null, 
                       dailySpents: Array<DailySpent> = null, 
                       transfers: Array<Transfer> = null){ 
    	this.month = month;
    	this.year = year;
    	this.salary = salary;
    	this.shortObjectif = shortObjectif;
    	this.objectif_id = objectif_id;
    	this.subscriptions = subscriptions;
        this.credits = credits;
        this.dailySpents = dailySpents;
    	this.transfers = transfers;
    	this.id = id;
    }

    public getId() : number{
    	return this.id;
    }
    public setId(id: number){
    	this.id = id;
    }

    public getMonth() : number{
    	return this.month;
    }
    public setMonth(month: number){
    	this.month = month;
    }

    public getYear(): number{
    	return this.year;
    }
    public setYear(year: number){
    	this.year = year;
    }

    public getSalary(): number{
    	return this.salary;
    }
    public setSalary(salary: number){
    	this.salary = salary;
    }

    public getShortObjectif(): number{
    	return this.shortObjectif;
    }
    public setShoartObjectif(shortObjectif: number){
    	this.shortObjectif = shortObjectif;
    }

    public getObjectifId(): number{
    	return this.objectif_id;
    }
    public setObjectifId(objectif_id: number){
    	this.objectif_id = objectif_id;
    }

    public getSubscriptions(){
    	return this.subscriptions;
    }
    public setSubscriptions(subscriptions : Array<Subscription>){
    	this.subscriptions = subscriptions;
    }

    public getCredits(){
    	return this.credits;
    }
    public setCredits(credits : Array<Credit>){
    	this.credits = credits;
    }

    public getDailySpents(){
        return this.dailySpents;
    }
    public setDailySpents(dailySpents : Array<DailySpent>){
        this.dailySpents = dailySpents;
    }

    public getTransfers(){
        return this.transfers;
    }
    public setTransfers(transfers : Array<Transfer>){
        this.transfers = transfers;
    }

    public clone(){
		return new Month(this.month,
    					 this.year,
    					 this.salary,
    					 this.shortObjectif,
    					 this.objectif_id,
                         this.id,
                         this.subscriptions,
                         this.credits,
                         this.dailySpents,
                         this.transfers);
	}

	public equals(month: Month){
		return (month.getId() == this.id) 
				&& (month.getMonth() == this.month) 
				&& (month.getYear() == this.year) 
				&& (month.getSalary() == this.salary) 
				&& (month.getShortObjectif() == this.shortObjectif) 
				&& (month.getObjectifId() == this.objectif_id)
				&& (month.getSubscriptions() == this.subscriptions)
                && (month.getCredits() == this.credits)
                && (month.getDailySpents() == this.dailySpents)
				&& (month.getTransfers() == this.transfers);
		
	}
    
    /********* Add elements to lists ***********************************/
    /******************************************************************/
    /**************** Subscription table *****************************/
    /****************************************************************/
    public addSubscription(subscription : Subscription){
        if(this.subscriptions == null || this.subscriptions == undefined)
            this.subscriptions = new Array<Subscription>();
        this.subscriptions.push(subscription);
    }

    public removeSubscription(subscription : Subscription){
        if(this.subscriptions == null || this.subscriptions == undefined)
            return;
        
        let index = this.subscriptions.indexOf(subscription);
        if(index > -1)
            this.subscriptions.splice(index, 1);
    }

    public updateSubscription(index: number, subscription : Subscription){
        if(this.subscriptions == null || this.subscriptions == undefined)
            return ;
        
        if(index < this.subscriptions.length && index >= 0)
            this.subscriptions[index] = subscription;
    }

    /******************************************************************/
    /******************** Credit table *******************************/
    /****************************************************************/

    public addCredit(credit : Credit){
        if(this.credits == null || this.credits == undefined)
            this.credits = new Array<Credit>();
        this.credits.push(credit);
    }

    public removeCredit(credit : Credit){
        if(this.credits == null || this.credits == undefined)
            return;
        
        let index = this.credits.indexOf(credit);
        if(index > -1)
            this.credits.splice(index, 1);
    }

    public updateCredit(index: number, credit : Credit){
        if(this.credits == null || this.credits == undefined)
            return ;
        
        if(index < this.credits.length && index >= 0)
            this.credits[index] = credit;
    }

    /******************************************************************/
    /**************** DailySpent table *******************************/
    /****************************************************************/

    public addDailySpent(dailySpent : DailySpent){
        if(this.dailySpents == null || this.dailySpents == undefined)
            this.dailySpents = new Array<DailySpent>();
        this.dailySpents.push(dailySpent);
    }

    public removeDailySpent(dailySpent : DailySpent){
        if(this.dailySpents == null || this.dailySpents == undefined)
            return;
        
        let index = this.dailySpents.indexOf(dailySpent);
        if(index > -1)
            this.dailySpents.splice(index, 1);
    }

    public updateDailySpent(index: number, dailySpent : DailySpent){
        if(this.dailySpents == null || this.dailySpents == undefined)
            return ;
        
        if(index < this.dailySpents.length && index >= 0)
            this.dailySpents[index] = dailySpent;
    }

    /******************************************************************/
    /****************** Transfer table *******************************/
    /****************************************************************/

    public addTransfer(transfer : Transfer){
        if(this.transfers == null || this.transfers == undefined)
            this.transfers = new Array<Transfer>();
        this.transfers.push(transfer);
    }

    public removeTransfer(transfer : Transfer){
        if(this.transfers == null || this.transfers == undefined)
            return;
        
        let index = this.transfers.indexOf(transfer);
        if(index > -1)
            this.transfers.splice(index, 1);
    }

    public updateTransfer(index: number, transfer : Transfer){
        if(this.transfers == null || this.transfers == undefined)
            return ;
        
        if(index < this.transfers.length && index >= 0)
            this.transfers[index] = transfer;
    }

}
    
