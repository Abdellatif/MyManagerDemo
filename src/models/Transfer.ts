/********************* Models ************************/
import { Spent } from './Spent'

export class Transfer extends Spent{
	//income = true ==> money in , income = false ==> money out 
	protected income : boolean;
	protected month_id : number;

	public constructor(title : string = null,
					   sum : number = null, 
					   description : string = null, 
					   income : boolean = null, 
					   month_id : number = null, 
					   id : number = null){
		super(title, sum, description, id);
		this.income = income;
		this.month_id = month_id;
	}

	public getMonthId() : number {
		return this.month_id;
	}

	public setMonthId(month_id : number){
		this.month_id = month_id;
	}

	public getIncome() : boolean {
		return this.income;
	}

	public setIncome(income : boolean){
		this.income = income;
	}

	public clone(){
		return new Transfer(this.title, 
							this.sum, 
							this.description, 
							this.income, 
							this.month_id, 
							this.id);
	}

	public equals(spent: Spent){
		if(spent instanceof Transfer)
			return (spent.getId() == this.id) 
					&& (spent.getTitle() == this.title) 
					&& (spent.getSum() == this.sum) 
					&& (spent.getDescription() == this.description) 
					&& (spent.getIncome() == this.income) 
					&& (spent.getMonthId() == this.month_id);
		return false;
	}
}