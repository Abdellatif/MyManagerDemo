export abstract class Spent{
	protected id : number;
	protected title : string;
	protected sum : number;
	protected description : string;

	public constructor(title : string = null, sum : number = null, description : string = null, id : number = null){
		this.id = id;
		this.title = title;
		this.sum = sum;
		this.description = description;
	}

	public getId() : number {
		return this.id;
	}

	public setId(id : number){
		this.id = id;
	}

	public getTitle() : string {
		return this.title;
	}

	public setTitle(title : string){
		this.title = title;
	}

	public getSum() : number {
		return this.sum;
	}

	public setSum(sum : number){
		this.sum = sum;
	}

	public getDescription() : string {
		return this.description;
	}

	public setDescription(description : string){
		this.description = description;
	}

	public abstract clone();

	public abstract equals(spent: Spent);

}