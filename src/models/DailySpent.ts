/********************* Models ************************/
import { Spent } from './Spent'

export class DailySpent extends Spent{
	
	protected date : Date;
	protected eSpent : number;
	protected restSpent : number;
	protected month_id : number;
	
	public constructor(title : string = null,
					   sum : number = null,
					   description : string = null, 
					   date : Date = null, 
					   eSpent : number = null, 
					   restSpent: number = null, 
					   month_id : number = null, 
					   id : number = null){
		super(title, sum, description, id);
		this.date = date;
		this.eSpent = eSpent;
		this.restSpent = restSpent;
		this.month_id = month_id;
	}

	public getMonthId() : number {
		return this.month_id;
	}

	public setMonthId(month_id : number){
		this.month_id = month_id;
	}

	public getDate() : Date {
		return this.date;
	}

	public setDate(date : Date){
		this.date = date;
	}

	public getESpent() : number {
		return this.eSpent;
	}

	public setESpent(eSpent : number){
		this.eSpent = eSpent;
	}

	public getRestSpent() : number {
		return this.restSpent;
	}

	public setRestSpent(restSpent : number){
		this.restSpent = restSpent;
	}

	public clone(){
		return new DailySpent(this.title,
							  this.sum,
							  this.description,
							  this.date,
							  this.eSpent,
							  this.restSpent,
							  this.month_id,
							  this.id);
	}

	public equals(spent: Spent){
		if(spent instanceof DailySpent)
			return (spent.getId() == this.id)
					 && (spent.getTitle() == this.title) 
					 && (spent.getSum() == this.sum) 
					 && (spent.getDescription() == this.description) 
					 && (spent.getDate() == this.date) 
					 && (spent.getESpent() == this.eSpent) 
					 && (spent.getRestSpent() == this.restSpent) 
					 && (spent.getMonthId() == this.month_id);
		return false;
	}
}