/********************* Models ************************/
import { Spent } from './Spent'
import { Month } from './Month'

export class Subscription extends Spent{
	protected months : Array<Month>;

	public constructor(title : string = null, 
					   sum : number = null, 
					   description : string = null, 
					   id : number = null, 
					   months : Array<Month> = null){
		super(title, sum, description, id);
		this.months = months;
	}

	public getMonths() {
		return this.months;
	}

	public setMonths(months : Array<Month>){
		this.months = months;
	}

	public clone(){
		return new Subscription(this.title, 
								this.sum, 
								this.description, 
								this.id,
								this.months);
	}

	public equals(spent: Spent){
		if(spent instanceof Subscription)
			return (spent.getId() == this.id) 
					&& (spent.getTitle() == this.title) 
					&& (spent.getSum() == this.sum) 
					&& (spent.getDescription() == this.description)
					&& (spent.getMonths() == this.months);
		return false;
	}

    /********* Add elements to lists ***********************************/
    /******************************************************************/
    /*********************** Month table *****************************/
    /****************************************************************/
    public addMonth(month : Month){
        if(this.months == null || this.months == undefined)
            this.months = new Array<Month>();
        this.months.push(month);
    }

    public removeMonth(month : Month){
    	if(this.months == null || this.months == undefined)
    		return;
    	
    	let index = this.months.indexOf(month);
      	if(index > -1)
        	this.months.splice(index, 1);
    }

    public updateMonth(index: number, month : Month){
    	if(this.months == null || this.months == undefined)
    		return ;
    	
    	if(index < this.months.length && index >= 0)
    		this.months[index] = month;
    }
}