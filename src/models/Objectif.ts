import { Month } from './Month'

export class Objectif{
	protected id : number;
    protected period : number;
	protected sum : number ;
    protected description : string;
    protected isDone : boolean;

    protected months : Array<Month>; 

    public constructor(period : number = null, 
    				   sum : number = null, 
    				   description : string = null, 
    				   isDone : boolean = null, 
    				   id : number = null,
    				   months : Array<Month> = null){
    	this.period = period;
    	this.sum = sum;
    	this.description = description;
    	this.isDone = isDone;
    	this.id = id;
    	this.months = months;
    }

    public getId() : number {
		return this.id;
	}

	public setId(id : number){
		this.id = id;
	}

	public getPeriod() : number {
		return this.period;
	}

	public setPerio(period : number){
		this.period = period;
	}

	public getSum() : number {
		return this.sum;
	}

	public setSum(sum : number){
		this.sum = sum;
	}

	public getDescription() : string {
		return this.description;
	}

	public setDescription(description : string){
		this.description = description;
	}

	public getStatus() : boolean {
		return this.isDone;
	}

	public setStatus(isDone : boolean){
		this.isDone = isDone;
	}

	public getMonths() {
		return this.months;
	}

	public setMonths(months : Array<Month>){
		this.months = months;
	}

	public clone(){
		return new Objectif(this.period,
    						this.sum,
    						this.description,
    						this.isDone,
    						this.id,
    						this.months);
	}

	public equals(objectif: Objectif){
		return (objectif.getId() == this.id)
			&& (objectif.getPeriod() == this.period) 
			&& (objectif.getSum() == this.sum) 
			&& (objectif.getDescription() == this.description) 
			&& (objectif.getStatus() == this.isDone)
			&& (objectif.getMonths() == this.months);
	}

    /********* Add elements to lists ***********************************/
    /******************************************************************/
    /*********************** Month table *****************************/
    /****************************************************************/
    public addMonth(month : Month){
        if(this.months == null || this.months == undefined)
            this.months = new Array<Month>();
        this.months.push(month);
    }

    public removeMonth(month : Month){
    	if(this.months == null || this.months == undefined)
    		return;
    	
    	let index = this.months.indexOf(month);
      	if(index > -1)
        	this.months.splice(index, 1);
    }

    public updateMonth(index: number, month : Month){
    	if(this.months == null || this.months == undefined)
    		return ;
    	
    	if(index < this.months.length && index >= 0)
    		this.months[index] = month;
    }
}
