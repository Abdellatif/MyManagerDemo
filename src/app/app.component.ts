/********************* Modules ************************/
import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen, SQLite } from 'ionic-native';
import { TranslateService } from 'ng2-translate/ng2-translate';

/********************* Components ************************/
import { AboutPage } from '../pages/about/about';
import { CreditsPage } from '../pages/credits/credits';
import { HistoricPage } from '../pages/historic/historic';
import { HomePage } from '../pages/home/home';
import { NewObjectifPage } from '../pages/new-objectif/new-objectif';
import { SubscriptionsPage } from '../pages/subscriptions/subscriptions';
import { TransfersPage } from '../pages/transfers/transfers';

/********************* Services ************************/
import { DBInit } from '../services/DBInit';

@Component({
  selector : 'side-rmenu',
  templateUrl: 'app.html'
})
export class MyApp {
   @ViewChild(Nav) nav: Nav;

  rootPage: any = NewObjectifPage;
  pages: Array<{title: string, component: any, icon: string}> = new Array();
  leftMenuTitle : string ;

  constructor(public platform: Platform,  public menu: MenuController, private translateService: TranslateService) {
    platform.ready().then(() => {
      this.initializeApp();
      this.initializeLeftMenu();
      
    });
    this.translateConfig();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  initializeLeftMenu(){
    //menu title
    this.translateService.get('TITLE').subscribe( value => { this.leftMenuTitle = value; });
  
    // pages
    this.translateService.get('HOME_TITLE').subscribe( value => { this.pages.push({ title: value, component: HomePage, icon:'home' }) });
    this.translateService.get('NEW_OBJECTIF_TITLE').subscribe( value => { this.pages.push({ title: value, component: NewObjectifPage, icon:'create' }) });
    this.translateService.get('HISTORIC_TITLE').subscribe( value => { this.pages.push({ title: value, component: HistoricPage, icon:'archive' }) });
    this.translateService.get('SUBSCRIPTIONS_TITLE').subscribe( value => { this.pages.push({ title: value, component: SubscriptionsPage, icon:'filing'  }) });
    this.translateService.get('CREDITS_TITLE').subscribe( value => { this.pages.push({title: value, component: CreditsPage, icon:'card' }) });
    this.translateService.get('TRANSFERS_TITLE').subscribe( value => { this.pages.push({ title: value, component: TransfersPage, icon:'cash' }) });
    this.translateService.get('ABOUT_TITLE').subscribe( value => { this.pages.push({title: value, component: AboutPage, icon:'information-circle' }) });      

  }

  translateConfig() {
        // var userLang = navigator.language.split('-')[0]; // use navigator lang if available
        // userLang = /(de|en|es)/gi.test(userLang) ? userLang : 'en';
        this.translateService.setDefaultLang('en');
        this.translateService.use('fr');
      }

  openPage(page) {

    this.menu.close();
    this.nav.setRoot(page.component);
  }
}

