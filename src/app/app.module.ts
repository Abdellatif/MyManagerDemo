import { NgModule, ErrorHandler } from '@angular/core';
import { Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { TranslateModule , TranslateStaticLoader, TranslateLoader} from 'ng2-translate/ng2-translate';
import { MyApp } from './app.component';

/********************* Service ************************/
import { CreditService } from '../services/CreditService';
import { DailySpentService } from '../services/DailySpentService';
import { MonthService } from '../services/MonthService';
import { ObjectifService } from '../services/ObjectifService';
import { SubscriptionService } from '../services/SubscriptionService';
import { TransferService } from '../services/TransferService';

/********************* Pages ************************/
import { AboutPage } from '../pages/about/about';
import { CreditsPage } from '../pages/credits/credits';
import { CreditFormPage } from '../pages/credit-form/credit-form';
import { HistoricPage } from '../pages/historic/historic';
import { HomePage } from '../pages/home/home';
import { NewObjectifPage } from '../pages/new-objectif/new-objectif';
import { SubscriptionsPage } from '../pages/subscriptions/subscriptions';
import { SubscriptionFormPage } from '../pages/subscription-form/subscription-form';
import { TransfersPage } from '../pages/transfers/transfers';
import { TransferFormPage } from '../pages/transfer-form/transfer-form';

import {Sql} from "../providers/Sql";


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    CreditsPage,
    CreditFormPage,
    HistoricPage,
    HomePage,
    NewObjectifPage,
    SubscriptionsPage,
    SubscriptionFormPage,
    TransfersPage,
    TransferFormPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    CreditsPage,
    CreditFormPage,
    HistoricPage,
    HomePage,
    NewObjectifPage,
    SubscriptionsPage,
    SubscriptionFormPage,
    TransfersPage,
    TransferFormPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Storage,
    CreditService,
    DailySpentService,
    MonthService,
    ObjectifService,
    SubscriptionService,
    TransferService,
    { provide: Sql, useClass: Sql },
  ]
})
export class AppModule {}

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}
